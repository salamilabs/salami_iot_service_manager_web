'use strict';

describe('Controller: KitchenmodalCtrl', function () {

  // load the controller's module
  beforeEach(module('salamiServiceManagerApp'));

  var KitchenmodalCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    KitchenmodalCtrl = $controller('KitchenmodalCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(KitchenmodalCtrl.awesomeThings.length).toBe(3);
  });
});
