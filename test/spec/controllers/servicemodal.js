'use strict';

describe('Controller: ServicemodalCtrl', function () {

  // load the controller's module
  beforeEach(module('salamiServiceManagerApp'));

  var ServicemodalCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ServicemodalCtrl = $controller('ServicemodalCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ServicemodalCtrl.awesomeThings.length).toBe(3);
  });
});
