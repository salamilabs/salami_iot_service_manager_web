'use strict';

/**
 * @ngdoc service
 * @name salamiServiceManagerApp.UserData
 * @description
 * # UserData
 * Service in the salamiServiceManagerApp.
 */
angular.module('salamiServiceManagerApp')
    .service('UserData', function () {

      this.init = function (loggedUser) {
        this.uri = loggedUser.uri;
        this.username = loggedUser.username;
        this.firstName = loggedUser.firstName;
        this.isActive = loggedUser.isActive;
        this.lastLogin = loggedUser.lastLogin;
        this.email = loggedUser.email;
        this.token = loggedUser.token;
      };

      this.reset = function () {
        this.uri = "";
        this.username = "";
        this.firstName = "";
        this.isActive = false;
        this.lastLogin = null;
        this.email = "";
        this.token = "";
      }
    });
