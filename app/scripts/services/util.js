'use strict';

/**
 * @ngdoc service
 * @name salamiServiceManagerApp.Util
 * @description
 * # Util
 * Service in the salamiServiceManagerApp.
 */
angular.module('salamiServiceManagerApp')
    .service('Util', function (UserData) {
      this.getAuthorizationHeader = function () {
        var token = UserData.token;
        var authHeader = {};

        if (token != null && token != undefined && token !== "") {
          authHeader['Authorization'] = "Bearer " + token;
        }
        return authHeader;
      };

      this.getAuthorizationParameter = function () {
        return {oauth_consumer_key: UserData.token}
      }
    });
