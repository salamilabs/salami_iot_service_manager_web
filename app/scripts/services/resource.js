'use strict';

/**
 * @ngdoc service
 * @name salamiServiceManagerApp.Resource
 * @description
 * # Resource
 * Factory in the salamiServiceManagerApp.
 */
angular.module('salamiServiceManagerApp')
    .factory('Resource', function (Restangular, Util) {

        var kitchens = Restangular.all('kitchen');
        var keys = Restangular.all("api_key");
        var devices = Restangular.all("device");
        var user = Restangular.all('user');
        return {
            getKitchens: function () {
                return kitchens.getList(Util.getAuthorizationParameter());
            },
            saveKitchen: function (newKitchen) {
                return kitchens.post(newKitchen, Util.getAuthorizationParameter());
            },
            updateKitchen: function (modifiedKitchen) {
                return modifiedKitchen.save(Util.getAuthorizationParameter());
            },
            deleteKitchen: function (toDeleteKitchen) {
                return this.updateKitchen(toDeleteKitchen);
            },
            getKeys: function () {
                return keys.getList(Util.getAuthorizationParameter());
            },
            getKeysByKitchen: function (kitchen) {
                var params = Util.getAuthorizationParameter();
                params.kitchen__name = kitchen.name;
                return keys.getList(params);
            },
            saveKey: function (newKey) {
                return keys.post(newKey, Util.getAuthorizationParameter());
            },
            updateKey: function (modifiedKey) {
                return modifiedKey.save(Util.getAuthorizationParameter());
            },
            deleteKey: function (toDeleteKey) {
                return this.updateKey(toDeleteKey);
            },
            getDevices: function () {
                return devices.getList(Util.getAuthorizationParameter());
            },
            updateDevice: function (modifiedDevice) {
                return modifiedDevice.save(Util.getAuthorizationParameter());
            },
            deleteDevice: function (toDeleteDevice) {
                return this.updateDevice(toDeleteDevice);
            },
            saveDevice: function (newDevice) {
                return devices.post(newDevice, Util.getAuthorizationParameter());
            },
            getUser: function () {
                return user.getList(Util.getAuthorizationParameter());
            },
            updateUser: function (modifiedUser) {
                return modifiedUser.save(Util.getAuthorizationParameter());
            }
        };
    });
