'use strict';

/**
 * @ngdoc service
 * @name salamiServiceManagerApp.Config
 * @description
 * # Config
 * Constant in the salamiServiceManagerApp.
 */
angular.module('salamiServiceManagerApp')
    .constant('Config', {
        clientId: 'gtJSgB1mvEpI4goMU9yumtpvsWR0FjqCTlzakuSJ',
        clientSecret: 'wm8OJ6hZKC04BEps4i2foHHMChzMbVlOxAIR9FxtiXPbOEBZQ1DvliieYcpS6zll0J1BeXpBBflh671c8sezWrYWoQtZzspxtTgbXaD2Lgc5lMVutifJ3w7USOc5zLt1',
        apiBaseUrl: 'http://127.0.0.1:8000/api/v1',
        tokenUrl: 'http://localhost:8000/o/token/',
        managerUrl: 'http://localhost:5000/methods'
    });
