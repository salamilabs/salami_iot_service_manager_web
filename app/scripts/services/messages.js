'use strict';

/**
 * @ngdoc service
 * @name salamiServiceManagerApp.Messages
 * @description
 * # Messages
 * Service in the salamiServiceManagerApp.
 */
angular.module('salamiServiceManagerApp')
    .service('Messages', function () {
      this.messages = [];

      this.addMessage = function (alertType, message) {
        if (this.messages.length == 3) {
          this.messages = [];
        }
        this.messages.push({type: alertType, msg: message});
      };

      this.removeMessage = function (index) {
        this.messages.splice(index, 1);
      };
    });
