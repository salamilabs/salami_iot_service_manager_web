'use strict';

/**
 * @ngdoc service
 * @name salamiServiceManagerApp.Model
 * @description
 * # Model
 * Factory in the salamiServiceManagerApp.
 */
angular.module('salamiServiceManagerApp')
    .factory('Model', function () {

        function RootLoggerData() {
            this.isEnabled = true;
            this.isRoot = true;
            this.id = "root";
            this.level = 20;
            this.handlers = [];
        }

        function LoggerData() {
            RootLoggerData.call(this);
            this.triggers = [];
        }

        LoggerData.prototype = new RootLoggerData();
        LoggerData.prototype.constructor = LoggerData;

        function HandlerData() {
            this.type = "Generic";
            this.isEnabled = true;
            this.level = 20;
            this.format = "%(levelname) -10s %(asctime)s %(module)s:%(lineno)s %(funcName)s %(message)s";
        }

        function FileHandlerData() {
            HandlerData.call(this);
            this.type = "FileHandler";
            this.filePath = ""
        }

        FileHandlerData.prototype = new HandlerData();
        FileHandlerData.prototype.constructor = FileHandlerData;

        function WsHandlerData() {
            HandlerData.call(this);
            this.type = "WsHandler";
            this.socketUrl = "";
        }

        WsHandlerData.prototype = new HandlerData();
        WsHandlerData.prototype.constructor = WsHandlerData;

        function Trigger(name) {
            if (name == undefined) {
                this.name = "New Trigger";

            } else {
                this.name = name;
            }
        }

        return {
            createLoggerData: function () {
                return new LoggerData();
            },
            createGenericHandlerData: function () {
                return new HandlerData();
            },
            createFileHandlerData: function () {
                return new FileHandlerData();
            },
            createWsHandlerData: function () {
                return new WsHandlerData();
            },
            createTrigger: function (name) {
                return new Trigger(name);
            }


        };
    });
