'use strict';

/**
 * @ngdoc service
 * @name salamiServiceManagerApp.Login
 * @description
 * # Login
 * Factory in the salamiServiceManagerApp.
 */
angular.module('salamiServiceManagerApp')
    .factory('Login', function ($http, $location, Config, UserData) {

        return {
            login: function (username, password) {
                var data = {
                    grant_type: 'password',
                    username: username, password: password
                };
                var auth = Config.clientId + ":" + Config.clientSecret;


                var req = {
                    method: 'POST',
                    url: Config.tokenUrl,
                    headers: {
                        'Authorization': 'Basic ' + btoa(auth),
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Accept': '*/*'
                    },
                    data: $.param(data)
                };
                return $http(req);
            },
            logout: function () {
                UserData.reset();
                sessionStorage.removeItem("loggedUser");
                $location.path("/login");
            }
        };
    });
