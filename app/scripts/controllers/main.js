'use strict';

/**
 * @ngdoc function
 * @name salamiServiceManagerApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the salamiServiceManagerApp
 */
angular.module('salamiServiceManagerApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
