'use strict';

/**
 * @ngdoc function
 * @name salamiServiceManagerApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the salamiServiceManagerApp
 */
angular.module('salamiServiceManagerApp')
    .controller('LoginCtrl', function ($scope, $log, $location, Login, Resource, UserData, Messages) {
        $scope.doLogin = function () {
            UserData.reset();
            Login.login($scope.username, $scope.password).then(function (response) {
                $log.debug("Server Response", response.data);
                var tokenInfo = response.data;
                UserData.token = tokenInfo.access_token;

                Resource.getUser().then(function (response) {
                    var user = response[0];
                    UserData.uri = user.resource_uri;
                    UserData.username = user.username;
                    UserData.email = user.email;
                    UserData.firstName = user.first_name;
                    UserData.isActive = user.is_active;
                    UserData.lastLogin = user.last_login;
                    $scope.username = UserData.username;
                    sessionStorage.loggedUser = JSON.stringify(UserData);
                    $location.path("/join");
                });
            }, function (error) {
                $log.error("Error - Cannot login", error);
                if (error.status == 401) {
                    Messages.addMessage('danger', 'Wrong username or password');
                } else {
                    Messages.addMessage('danger', 'Error - Cannot login');
                }

            });

        }
    });
