'use strict';

/**
 * @ngdoc function
 * @name salamiServiceManagerApp.controller:ServicemodalCtrl
 * @description
 * # ServicemodalCtrl
 * Controller of the salamiServiceManagerApp
 */
angular.module('salamiServiceManagerApp')
  .controller('ServicemodalCtrl', function ($scope, $uibModalInstance, service) {
      $scope.service = service;

      $scope.ok = function () {
          $uibModalInstance.close($scope.service);
      };

      $scope.cancel = function () {
          $uibModalInstance.dismiss('cancel');
      };
  });
