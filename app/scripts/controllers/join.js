'use strict';

/**
 * @ngdoc function
 * @name salamiServiceManagerApp.controller:JoinCtrl
 * @description
 * # JoinCtrl
 * Controller of the salamiServiceManagerApp
 */
angular.module('salamiServiceManagerApp')
    .controller('JoinCtrl', function ($scope, $log, $location, $http, $uibModal, Config, Resource, UserData, Messages) {

        $scope.selectedKitchen = null;

        loadKitchens();

        $scope.doJoin = function () {
            var content = {"toi_url": "http://www.ilcantierehitech.it", "socket_url": "ws://127.0.0.1:8888"};
            var params = {"params": {"key": $scope.selectedKitchen.api_key.api_key, "content": content}};
            var data = {
                "jsonrpc": "2.0",
                "method": "Manager.join",
                "params": params,
                "id": "1"
            };

            var req = {
                method: 'POST',
                url: Config.managerUrl,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: data
            };
            $http(req).then(function (response) {
                var data = response.data;
                if (data.result.status === "OK") {
                    var Device  = {};
                    Device.name = "Bla";
                    Device.description = "Bla bla bla";
                    Device.api_key = $scope.selectedKitchen.api_key.resource_uri;
                    Device.passport = data.result.path;
                    Resource.saveDevice(Device);
                    $location.path("/finish");
                } else {
                    $log.error("Error - Cannot join device", data.error);
                    Messages.addMessage('danger', 'Error - Cannot join device');
                }
            });
        };

        function loadKitchens() {
            Resource.getKitchens().then(function (kitchens) {
                $scope.kitchens = kitchens;
                $log.info("Kitchen List: ", $scope.kitchens);
            }, function (error) {
                $log.error("Error - Cannot load data from server", error);
                Messages.addMessage('danger', 'Error - Cannot load data from server');
            });
        }

        // Kitchen Modal
        $scope.openKitchenModal = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'kitchenmodalcontent.html',
                controller: 'KitchenmodalCtrl',
                size: ''
            });

            modalInstance.result.then(function (kitchen) {

                kitchen.owner = UserData.uri;
                Resource.saveKitchen(kitchen).then(function () {
                    Messages.addMessage('success', 'Kitchen correctly created');
                    loadKitchens();
                }, function (error) {
                    $log.error("Error - Cannot create kitchen", error);
                    Messages.addMessage('danger', 'Error - Cannot create kitchen');
                });

            }, function () {
                $log.debug('Modal dismissed at: ' + new Date());
            });
        };

    });
