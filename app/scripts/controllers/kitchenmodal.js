'use strict';

/**
 * @ngdoc function
 * @name salamiServiceManagerApp.controller:KitchenmodalCtrl
 * @description
 * # KitchenmodalCtrl
 * Controller of the salamiServiceManagerApp
 */
angular.module('salamiServiceManagerApp')
    .controller('KitchenmodalCtrl', function ($scope, $uibModalInstance) {
        $scope.kitchen = {};

        $scope.ok = function () {
            $uibModalInstance.close($scope.kitchen);
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    });
