'use strict';

/**
 * @ngdoc function
 * @name salamiServiceManagerApp.controller:NotificationsCtrl
 * @description
 * # NotificationsCtrl
 * Controller of the salamiServiceManagerApp
 */
angular.module('salamiServiceManagerApp')
    .controller('NotificationsCtrl', function ($scope, $interval, Messages) {
        $scope.$watchCollection(function () {
            return Messages.messages;
        }, function (newValue) {
            $scope.resetAlertTimer();
            $scope.alerts = newValue;
        });

        $scope.resetAlertTimer = function () {
            $interval.cancel($scope.stop);
            $scope.startAlertTimer();
        };

        $scope.startAlertTimer = function () {
            $scope.stop = $interval(function () {
                $scope.alerts = []
            }, 30000);
        };

        $scope.closeAlert = function (index) {
            Messages.removeMessage(index);
        };

        $scope.startAlertTimer();
    });
