'use strict';

/**
 * @ngdoc function
 * @name salamiServiceManagerApp.controller:MenuCtrl
 * @description
 * # MenuCtrl
 * Controller of the salamiServiceManagerApp
 */
angular.module('salamiServiceManagerApp')
  .controller('MenuCtrl', function ($scope, $location) {
      $scope.isInit = function () {
          if ($location.path() === "/init" || $location.path() === "/join") {
              return true;
          }
      };
  });
