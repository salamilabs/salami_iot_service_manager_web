'use strict';

/**
 * @ngdoc function
 * @name salamiServiceManagerApp.controller:FinishCtrl
 * @description
 * # FinishCtrl
 * Controller of the salamiServiceManagerApp
 */
angular.module('salamiServiceManagerApp')
    .controller('FinishCtrl', function ($scope, $log, $location, $http, Config) {
        $scope.startServices = true;
        $scope.doFinish = function () {
            var data = {
                "jsonrpc": "2.0",
                "method": "Manager.startServices",
                "id": "1"
            };

            if ($scope.startServices) {
                var req = {
                    method: 'POST',
                    url: Config.managerUrl,
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: data
                };
                $http(req).then(function (response) {
                    var data = response.data;
                });
            }
            $location.path("/");
        }
    });
