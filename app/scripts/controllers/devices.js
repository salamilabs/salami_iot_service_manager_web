'use strict';

/**
 * @ngdoc function
 * @name salamiServiceManagerApp.controller:DevicesCtrl
 * @description
 * # DevicesCtrl
 * Controller of the salamiServiceManagerApp
 */
angular.module('salamiServiceManagerApp')
    .controller('DevicesCtrl', function ($scope, $log, $websocket) {
        $scope.slices = [];
        //TODO: remove hardcoded url
        var dataStream = $websocket('ws://127.0.0.1:8888/system?id=ALL');
        dataStream.onMessage(function (jsonMessage) {
            var message = JSON.parse(jsonMessage.data);
            //TODO: Create javascript SalamiConstants
            if (message.command == 210) {
                var commandData = message.command_data;
                $log.info("Received message is: ", message);
                var slice = {};
                slice.id = commandData.slot;
                slice.name = commandData.toi.name;
                slice.author = commandData.toi.author;
                slice.version = commandData.toi.version;
                slice.status = "Waiting for approval";
                $scope.slices.push(slice);
            }
        });

        $scope.enableDevice = function (device) {

        };

        $scope.disableDevice = function (device) {

        };
    });
