'use strict';

/**
 * @ngdoc function
 * @name salamiServiceManagerApp.controller:ServicesCtrl
 * @description
 * # ServicesCtrl
 * Controller of the salamiServiceManagerApp
 */
angular.module('salamiServiceManagerApp')
    .controller('ServicesCtrl', function ($scope, $log, $http, $uibModal, Config) {
        loadServices();

        $scope.startService = function (service) {
            $log.info("Service ", service);
            var params = {"service_id": service};
            var data = {
                "jsonrpc": "2.0",
                "method": "Manager.startService",
                "params": params,
                "id": "1"
            };

            var req = {
                method: 'POST',
                url: Config.managerUrl,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: data
            };
            $http(req).then(function (response) {
                var data = response.data;
                loadServices();
            });
        };

        $scope.stopService = function (service) {
            $log.info("Service ", service);
            var params = {"service_id": service};
            var data = {
                "jsonrpc": "2.0",
                "method": "Manager.stopService",
                "params": params,
                "id": "1"
            };

            var req = {
                method: 'POST',
                url: Config.managerUrl,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: data
            };
            $http(req).then(function (response) {
                var data = response.data;
                loadServices();
            });
        };

        $scope.openServiceModal = function (service) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'servicemodalcontent.html',
                controller: 'ServicemodalCtrl',
                size: '',
                resolve: {service: service}
            });

            modalInstance.result.then(function (service) {
                var data = {};
                data.address = service.configuration.address;
                data.port = service.configuration.port;
                $http({
                    method: 'PUT',
                    url: "http://localhost:5000/services/configuration/" + service.id,
                    data: $.param(data),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).then(function (response) {
                    $log.info("Oh Yeah!");
                })
            }, function () {
                $log.debug('Modal dismissed at: ' + new Date());
            });
        };

        function loadServices() {
            $http.get("http://localhost:5000/services/").then(function (response) {
                $scope.services = response.data;
            });
        }
    });
