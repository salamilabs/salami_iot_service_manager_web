'use strict';

/**
 * @ngdoc function
 * @name salamiServiceManagerApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the salamiServiceManagerApp
 */
angular.module('salamiServiceManagerApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
