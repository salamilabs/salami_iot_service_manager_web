'use strict';

/**
 * @ngdoc function
 * @name salamiServiceManagerApp.controller:InitCtrl
 * @description
 * # InitCtrl
 * Controller of the salamiServiceManagerApp
 */
angular.module('salamiServiceManagerApp')
    .controller('InitCtrl', function ($scope, UserData) {
        $scope.isLogged = false;
        $scope.$watch(function () {
            return UserData.uri;
        }, function () {
            $scope.isLogged = UserData.uri != undefined;
        });
    });
