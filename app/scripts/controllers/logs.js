'use strict';

/**
 * @ngdoc function
 * @name salamiServiceManagerApp.controller:LogsCtrl
 * @description
 * # LogsCtrl
 * Controller of the salamiServiceManagerApp
 */
angular.module('salamiServiceManagerApp')
    .controller('LogsCtrl', function ($scope, $http, $log, $uibModal, Model) {

        // Root Logger
        var rootLoggerData = Model.createLoggerData();

        var rootFilehandler = Model.createFileHandlerData();
        rootFilehandler.filePath = "";
        rootFilehandler.level = rootLoggerData.level;
        rootLoggerData.handlers.push(rootFilehandler);

        // MasterChef Logger
        var masterChefLoggerData = Model.createLoggerData();
        masterChefLoggerData.id = "MASTER-CHEF";
        masterChefLoggerData.isRoot = false;
        masterChefLoggerData.level = 30;
        masterChefLoggerData.isEnabled = false;

        var masterChefFileHandlerData = Model.createFileHandlerData();
        masterChefFileHandlerData.filePath = "";
        masterChefFileHandlerData.level = masterChefLoggerData.level;
        masterChefLoggerData.handlers.push(masterChefFileHandlerData);

        masterChefLoggerData.triggers.push(Model.createTrigger("subscriber"));
        masterChefLoggerData.triggers.push(Model.createTrigger("clock"));
        masterChefLoggerData.triggers.push(Model.createTrigger("Task"));

        // Assistant Chef Logger
        var assistantChefLoggerData = Model.createLoggerData();
        assistantChefLoggerData.id = "assistant-chef";
        assistantChefLoggerData.isRoot = false;
        assistantChefLoggerData.isEnabled = true;

        var assistantChefFileHandler = Model.createFileHandlerData();
        assistantChefFileHandler.filePath = "";
        assistantChefFileHandler.level = assistantChefLoggerData.level;

        var assistantChefWsHandler = Model.createWsHandlerData();
        assistantChefWsHandler.socketUrl = "";
        assistantChefWsHandler.level = assistantChefLoggerData.level;

        assistantChefLoggerData.handlers.push(assistantChefFileHandler);
        assistantChefLoggerData.handlers.push(assistantChefWsHandler);

        assistantChefLoggerData.triggers.push(Model.createTrigger("worker"));

        $scope.loggers = [];
        $scope.loggers.push(rootLoggerData);
        $scope.loggers.push(masterChefLoggerData);
        $scope.loggers.push(assistantChefLoggerData);

        $scope.currentLoggerContent = "ERROR      2016-02-19 18:25:23,087 subscriber:55 on_error Websocket error occurred: [Errno 111] Connection refused";

        $scope.removeHandler = function (index, logger) {
            for (var i in $scope.loggers) {
                var current = $scope.loggers[i];

                if (current.id == logger.id) {
                    current.handlers.splice(index, 1);
                }
                else if (current.id == logger.id) {
                    current.handlers.splice(index, 1);
                }
                else if (current.id == logger.id) {
                    current.handlers.splice(index, 1);
                }
            }
        };

        $scope.removeTrigger = function (index, logger) {
            for (var i in $scope.loggers) {
                var current = $scope.loggers[i];

                if (current.id == logger.id) {
                    current.triggers.splice(index, 1);
                }
                else if (current.id == logger.id) {
                    current.triggers.splice(index, 1);
                }
                else if (current.id == logger.id) {
                    current.triggers.splice(index, 1);
                }
            }
        };

        $scope.viewLogs = function (loggerId) {
            //var ws = $websocket.$new(Endpoint.ws + "?id=" + loggerId + ":log");

            //ws.$on('$open', function () {
            //    console.log('Oh my gosh, websocket is really open! Fukken awesome!');
            //});

            //ws.$on('$message', function (data) {
            //    $scope.currentLoggerContent = $scope.currentLoggerContent + data;
            //});

        };

        // Handler modal
        $scope.openHandlerModal = function (handler, logger) {
            if (handler == undefined) {
                handler = Model.createGenericHandlerData();
            }

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'handlerModalContent.html',
                controller: 'LoggerhandlermodalinstancectrlCtrl',
                size: '',
                resolve: {
                    handler: function () {
                        return handler;
                    },
                    logger: function () {
                        return logger;
                    }
                }
            });

            modalInstance.result.then(function () {
                $log.info('Modal dismissed at: ' + new Date());
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        // Trigger modal
        $scope.openTriggerModal = function (trigger, logger) {
            if (trigger == undefined) {
                trigger = Model.createTrigger();
            }

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'triggerModalContent.html',
                controller: 'LoggertriggermodalinstancectrlCtrl',
                size: '',
                resolve: {
                    trigger: function () {
                        return trigger;
                    },
                    logger: function () {
                        return logger;
                    }
                }
            });

            modalInstance.result.then(function () {
                $log.info('Modal dismissed at: ' + new Date());
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

    });
