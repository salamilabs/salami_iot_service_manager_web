'use strict';

/**
 * @ngdoc overview
 * @name salamiServiceManagerApp
 * @description
 * # salamiServiceManagerApp
 *
 * Main module of the application.
 */
angular
    .module('salamiServiceManagerApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'ui.bootstrap',
        'restangular',
        'ngWebSocket'
    ])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/main.html',
                controller: 'MainCtrl',
                controllerAs: 'main'
            })
            .when('/devices', {
                templateUrl: 'views/devices.html',
                controller: 'DevicesCtrl',
                controllerAs: 'devices'
            })
            .when('/settings', {
                templateUrl: 'views/settings.html'
            })
            .when('/init', {
                templateUrl: 'views/init.html',
                controller: 'InitCtrl',
                controllerAs: 'init'
            })
            .when('/join', {
                templateUrl: 'views/join.html',
                controller: 'JoinCtrl',
                controllerAs: 'join'
            })
            .when('/finish', {
                templateUrl: 'views/finish.html',
                controller: 'JoinCtrl',
                controllerAs: 'join'
            }).when('/services', {
                templateUrl: 'views/services.html',
                controller: 'ServicesCtrl',
                controllerAs: 'services'
            })
            .when('/logs', {
                templateUrl: 'views/logs.html',
                controller: 'LogsCtrl',
                controllerAs: 'logs'
            })
            .otherwise({
                redirectTo: '/'
            });
    }).config(function (RestangularProvider, Config) {
    RestangularProvider.setBaseUrl(Config.apiBaseUrl);
    RestangularProvider.setRequestSuffix("/");
    RestangularProvider.setExtraFields(['name']);
    RestangularProvider.setDefaultRequestParams('get', {format: 'json', 't': new Date().getTime()});
    RestangularProvider.setResponseExtractor(function (response, operation) {
        return response.objects;
    });
}).run(function ($rootScope, $location, $http, Config, UserData) {
    var data = {
        "jsonrpc": "2.0",
        "method": "Manager.isFirstAccess",
        "params": {},
        "id": "1"
    };

    var req = {
        method: 'POST',
        url: Config.managerUrl,
        headers: {
            'Content-Type': 'application/json'
        },
        data: data
    };
    var respo = $http(req);

    $rootScope.$on("$routeChangeStart", function (event, next, current) {
        respo.then(function (response) {
            var isLogged = UserData.token;
            if (response.data.result === 'true' && isLogged === undefined) {
                $location.path("/init");
            }
        });
    });
});
